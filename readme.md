# Criterios de instalación

Este documento pretende ser una guía sencilla para los instaladores que participaran en FLISOL, pero a su vez se espera que pueda ser de gran ayuda a aquellos que quieran incursionar en la instalación y/o uso de sistemas GNU/Linux. El mismo no ahondará en detalles técnicos, pero sin dejar de lado el uso de palabras técnicas sencillas que de todas formas uno puede investigar para profundizar en detalle.
Antes de empezar cabe recalcar que se tomara en cuenta que el uso final del sistema irá destinado a personas recién iniciadas. Esto debido a las características del evento FLISOL que intenta acercar a más personas al uso y conocimiento del software libre. Entonces no se pretende, en principio, hacer instalación de sistemas complejos y/o que dificulten el uso del mismo. Pero sí busca que el mismo permita adquirir conocimientos y/o aprendizajes (ya sea por lo teórico o lo practico) sin ningún tipo de limitaciones.
Dicho todo esto: buena instalación @-@/

## Distros recomendadas

Esta es una lista de distros recomendadas:

* Linux Mint
* EndeavourOS
* Manjaro
* Loc-OS
* MX Linux
* Antix Linux

Estas distros son recomendadas tanto por su facilidad de uso como la baja exigencia de uso de recursos (CPU, RAM, almacenamiento en disco).
Nota: estas son las distros recomendadas hasta el día de la fecha 17/04/2024. Esta lista es recomendable actualizarla, ya que las distros van cambiando con el tiempo. Por ej. ya ha dejado de ser recomendable distros como Ubuntu debido a su exceso de uso de recursos y por las limitaciones que puede llegar a presentar.

## Instalación

En principio, la mayoría de las distribuciones GNU/Linux modernas destinada a usuarios iniciados suelen tener un asistente de instalación que le facilitara hacer configuraciones iniciales (idioma, teclado, conexión, etc.). El acceso, en la mayoría de estas, se encuentran tanto en un icono de escritorio (llámese "Instalación" o lo que corresponda) como bien puede ser un solo comando en la terminal. Si esto representa una dificultad en algún momento: nunca dude en consultar a otro instalador o bien consulte la web de la distro que se pretende instalar.
Antes de continuar, no está de más recordar que existe la posibilidad que los datos se pierdan (ya sea porque se hace un borrado completo del disco o bien por algún error de instalación que se haga en el camino). Por ende se aconseja a los instaladores aclarar esto a los usuarios y consultar si ha hecho un backup. Si eres un nuevo usuario que está leyendo esta guía: se aconseja que hagas un backup de tus archivos importantes.
Y por último, ni mucho menos importante, el instalador debe consultar y mencionar al usuario como sé ira instalando el sistema. Esto garantiza que ambas partes aprendan como también el usuario esté informado de que pasara con su computadora.

### Pre-instalación

Antes de comenzar con los siguientes pasos: usted debe poseer un dispositivo de almacenamiento (pendrive, SSD, HDD, etc) en donde almacenar una distro y que dicho dispositivo pueda ser booteable. Actualmente, las herramientas más recomendables para dicho propósito son:

* Ventoy: la misma permite tener varios sistemas en un mismo dispositivo sin mucho problema ni pasos intermedios.
* Balena Etcher: permite tener solo un sistema en el dispositivo. Muy recomendable para empezar por su facilidad de uso.

También se pueden mencionar otros como "Rufus", "UNetbootin" y "Yumi", que pueden ser más o menos efectivos que las otras opciones (esto varía por diferentes factores que a veces pueden resultar un tanto desconocidos). La forma de instalar y usar las herramientas mencionadas quedará en manos del lector adquirir dicho conocimiento (después de todo cada una tiene un manual en sus respectivas páginas).
También quedará a cargo del lector como entrar a las opciones de la BIOS/UEFI para poder habilitar el arranque desde otros dispositivos de almacenamiento al iniciar la PC.
Una vez preparado el dispositivo, como instalador: puede mostrar al usuario como funciona la distro antes de proceder con la instalación (mostrarle el entorno, que herramientas posee, darle un bosquejo de como instalar programas y lo que considere pertinente).

### Particiones recomendadas

Las particiones recomendadas en caso de que la computadora tenga BIOS:

* 30GB o más en formato EXT4 para / (root)
* En caso de poseer un disco rígido (HDD):
  
  - Si tiene 1GB de RAM: una partición SWAP de 2GB
  - Si tiene RAM => 2GB y RAM < 4GB: una partición SWAP igual a RAM x2
  - Si tiene RAM => 4GB: una particion SWAP igual a RAM o bien 0GB
* El resto: formato EXT4 para /home

Las particiones recomendadas en caso de que la computadora tenga UEFI:

* 512MB en formato fat32 con flags "esp, boot" para /boot/EFI
* 30GB o màs en formato EXT4 para / (root)
* En caso de poseer un disco rígido (HDD):
  - Si tiene 1GB de RAM: una partición SWAP de 2GB
  - Si tiene RAM => 2GB y RAM < 4GB: una partición SWAP igual a RAM x2
  - Si tiene RAM => 4GB: una partición SWAP igual a RAM o bien 0GB
* El resto: formato EXT4 para /home

Aclaración: el orden está escrito para facilitar el cálculo del tamaño de las particiones. Pero se aconseja el siguiente orden de particiones en ambos casos: /boot/EFI, /, /home, SWAP.

Las particiones y disposición de las mismas están pensadas para tener al sistema operativo separado de los datos y configuraciones del usuario. Esto representa una ventaja al momento de presentarse algún problema con el sistema, se deba hacer un backup de los datos y/o bien pasar las configuraciones a otro medio.

Se aconseja el uso de "Gparted" para poder realizar y visualizar de forma más cómoda las particiones.

Si bien esto es posible que sea algo nuevo para un instalador recién iniciado: no dude, como siempre, consultar a otro instalador. Pero en caso de que pueda representar un inconveniente o hasta incluso dificultosa la instalación del sistema con esta disposición de particiones: puede hacer uso de la instalación automática que proporcione la distro a instalar. Esto último no es lo tan aconsejable debido a que no tiene las ventajas anteriormente mencionadas, pero puede sacar de un apuro a más de uno.

En caso de querer realizar un Dual Boot (tener dos o más sistemas instalados en disco): también se puede realizar el mismo tipo de disposición de particiones, pero quedará a criterio del instalador como maniobrar con las particiones existentes. Pero en caso de volverse complejo, en este caso, es aconsejable el uso de una instalación automática (por lo general existe la opción "Instalar con \[nombre del otro sistema]").

En otros casos no mencionados (por ej. un disco con poca capacidad) o contemplados: instalador flisolero... haz tu magia.

### Post instalación

Luego de haber realizado la instalación del sistema operativo: es aconsejable hacer una actualización del sistema (si es que el establecimiento posee de conexión a internet). Esto va a variar de distro a distro. Por lo general suelen ser, para las distros más conocidas, los siguientes comandos:

* Para distros basadas en Debian:
  ```
  sudo apt update && sudo apt upgrade
  ```
* Para distros basadas en Arch:
  ```
  sudo pacman -Syyu
  ```
En caso de que no funcionen o no correspondan a las distros mencionadas: consulte la documentación o la web de la distro instalada.

Por otro lado, también puede ocurrir que falte la instalación de algunos drivers y/o programas para el correcto funcionamiento del sistema en la computadora. Se aconseja revisar el correcto funcionamiento de lo que se mencionara acontinuacion al ser de los errores/fallos más comunes:

* Conexión de red
* Conexión de red WiFi
* Bajar y levantar la pantalla de una notebook/netbook/laptop
* Funcionamiento de todos los puertos
* Funcionamiento correcto de los periféricos (teclado, mouse, otros)
* Funcionamiento correcto del apartado gráfico (pantalla)

El como solucionar los problemas que se presenten ya dependerá de la habilidad y los conocimientos del instalador. En caso de no encontrar una solución viable es aconsejable intentar con otra distro.
Por último, no olvide dar recomendaciones y compartir conocimientos útiles al usuario.
Tampoco olvide que toda computadora es un mundo: no exijas mucho a la máquina ni tampoco te exijas tanto. Y que toda persona es un mundo: sé paciente y recuerda que todos estuvimos allí... empezando desde cero.

# Preguntas frecuentes

* Soy nuevo, ¿puedo participar aunque nunca hice una instalación?
  - Sí, por supuesto. En Flisol te encontrarás con otras personas (instaladores registrados o no) que pueden enseñarte.

* Bien, llegue al evento, pero no traje un almacenamiento USB, ¿qué hago? ¿Me vuelvo a mi casa y busco uno?
  - Si vives cerca puedes hacerlo. Pero no es necesario, seguro alguna otra persona podrá prestarte uno o hay de sobra en el evento.

* Tengo un USB, pero no cargue ninguna ISO, ¿me vuelvo a mi casa?
  - Lo mismo que en el apartado anterior. Seguro alguna persona pueda pasarte alguna ISO.

* Ya me cansé de hacer instalaciones, ¿ya me puedo ir a mi casa?
  - Sí, puedes. Aunque en Flisol se dan charlas y hay mucha gente con quien conversar. Tal vez darse una vuelta sea una buena idea.

* Soy nuevo y me trabé con la instalación, ¿qué hago?
  - No olvides que hay mas instaladores en el evento: no dudes en consultarles.

* Esas distros no son 100% libres, ¿dónde están las distros 100% libreees?
  - La idea del Flisol es instalar GNU/Linux a usuarios que quieren iniciar en este mundillo. Entonces las razones pueden ser simplemente porque no cumple con lo mencionado en el apartado de "Distros Recomendadas". Si bien quisiéramos que se instalaran distros 100% libres, la realidad es que todos al iniciar con GNU/Linux comenzamos desde 0.

* La distro que suelo recomendar no está entre las recomendadas, ¿por qué?
  - Esto puede ser por dos razones: dejo de recomendarse hace tiempo o bien la lista no está actualizada. Si se da el primer caso es aconsejable informarse (ya sea preguntando o bien buscando artículos relacionados). Si luego de haberse informado no encuentra razones por la cual no deba ser recomendada una distro: puede informar a otros instaladores o bien modificar (o recomendar la modificación de) este archivo.
